#ifndef _FILEPO_H_
#define _FILEPO_H_
 
#define OUTPUT_PATH "../OUTPUT/"
#define OUTPUT_RESULT "results.txt"
#define BENCHMARKS_FILE "benchmarks"

int get_num_files(char *input_path);
void get_files_from_dir(FILE **files, int num_files, char *input_path);
long get_file_size (FILE *file);
long get_total_files_size (FILE **files, int num_files);
void assign_files (float *assignment_points, FILE **files, int num_files, long total_size, int np);

#endif