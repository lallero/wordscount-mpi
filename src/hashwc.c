#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include "hashwc.h"
#include "filepo.h"

/**
	Hash table, functions applying the linear probing strategy for the collision problem
*/

/**
	Simple hash function sized at WORDSNUMBER
*/
unsigned long sized_hash(char *s){

    unsigned long hashval = 5381;
    int c;
    while ((c = *s++))
        hashval = ((hashval << 5) + hashval) + c;

    return hashval%WORDSNUMBER;
}

/**
	Initializes an hash maps to zeros occurrences
*/
void init_hash_table(hash_e *hash_ground) {

	for (int i = 0; i < WORDSNUMBER; i++)
		hash_ground[i].counter = 0;
	return;
}

/**
	According to the linear probing, this function searches a word
	in the given hash map by its key, if the slot is occupied (collision),
	it searches it in the next slots, iteratively. If it runs up against a 
	free slot it means that the word has not beed added yet and it has to
	be added exactly in that current free slot.
	So the return value is the address of the found element or
	the address of the first free slot
*/
hash_e* find_word_or_next_free_slot(char *word, hash_e *hash_ground) {
	unsigned long position = sized_hash(word);
	while (hash_ground[position].counter != 0) {
		if (strcmp(word, hash_ground[position].value) == 0)
			return &hash_ground[position];
		else
			position = (position + 1) % WORDSNUMBER;
	}
	return &hash_ground[position];

}

/**
	Increments the occurrences of a word in the given hashmap by the
	occ value given in input. If it doesn't exist it gets added/created
*/
void add_or_incr(char *str, int occ, hash_e *hash_ground) {
	hash_e *ele = (hash_e *) find_word_or_next_free_slot(str, hash_ground);

	if (ele->counter != 0)
		ele->counter = ele->counter + occ;
	else {
		strcpy(ele->value, str);
		ele->counter = occ;
	}
	return;
}

/**
	Merges the hashtable hg2 in the hashtable hg1. It correctly merges the 
	occurrences of the same words stored in different positions
*/
void merge_h_tables(hash_e *hg1, hash_e *hg2) {
	for (int i = 0; i < WORDSNUMBER; i++) {
		if (hg2[i].counter != 0)
			add_or_incr(hg2[i].value, hg2[i].counter, hg1);
	}
}

/**
	Reads "size" chars from the "buffer", isolates the words 
	and add them to the hashmap "hash_ground"
*/
void count_words(hash_e *hash_ground, char *buffer, long size) {
	char current_word[WORDLENGHT];
	int lol = 0;

	for (long i = 0; i < size + 1; i++) {
		if (buffer[i]>96 && buffer[i]<123) {
			current_word[lol] = buffer[i];
			lol++;
		}
		else if(buffer[i]>64 && buffer[i]<91){
			buffer[i] = buffer[i] + 32;
			current_word[lol] = buffer[i];
			lol++;
		}
		else 
			if (lol != 0) {
				current_word[lol] = '\0';
				add_or_incr(current_word, 1, hash_ground);
				lol = 0;
			}
	}
}


/**
	Auxiliary function used to avoid truncating words. It moves the file pointer in 
	input to the beginning of the next word in case it points in the middle of a word
*/
int do_not_truncate_word(FILE *file){
	if (ftell(file) == 0)
		return 0;
	fseek(file, -1, SEEK_CUR);
	int i = -1;
	char c = fgetc(file);
	while ((c > 96 && c < 123) || (c>64 && c<91)) {
		c = fgetc(file);
		i++;
	}
	return i;
}

/**
	It takes in input starting_point and ending_point:
		- the integer part indicates the file number
		- the decimal part indicates the percetage of file
	The function interpretes these values converting them to exact file pointers.
	It manages all the cases: variable files number with truncations (or not)
	Once assured the words aren't truncated it invokes count_words() to count the words
*/
void count_words_from_files(hash_e *hash_ground, FILE **files, float starting_point, float ending_point) {
	//float current_point = starting_point;
	int num_files = (int)ending_point - (int)starting_point + 1;
	int current_file = (int)starting_point;
	
	// Let's define correctly the starting point
	if (starting_point - (int)starting_point != 0) {
		do_not_truncate_word(files[current_file]);
	}

	if ((ending_point - (int)ending_point) == 0)
		num_files--;

	char *buffer;
	long offset;
	for ( ; current_file < (int)starting_point + num_files; current_file++) {
		// We define the offset for each basing on the ending point
		if (current_file == (int)ending_point) {
			offset = (long)((ending_point - (int)ending_point) * get_file_size(files[current_file]));
			fseek(files[current_file], offset, SEEK_SET);
			offset = offset + do_not_truncate_word(files[current_file]);
			fseek(files[current_file], 0, SEEK_SET);
		}
		else {
			offset = get_file_size(files[current_file]);
		}
		// We calculate the real starting point for each file basing on the starting file point marker
		if ((current_file == (int)starting_point) && (starting_point - (int)starting_point != 0)) {
			long start = (long)((starting_point - (int)starting_point) * get_file_size(files[current_file]));
			fseek(files[current_file], start, SEEK_CUR);
			do_not_truncate_word(files[current_file]);
		}
		else
			fseek(files[current_file], 0, SEEK_CUR);
		offset = offset - ftell(files[current_file]);

		buffer = malloc(offset*sizeof(char) + 1);
		size_t newLen = fread(buffer, sizeof(char), offset, files[current_file]);
	    if ( ferror( files[current_file] ) != 0 )
	        fputs("Error reading file", stderr);
	    else
	        buffer[newLen++] = '\0';

	    count_words(hash_ground, buffer, offset);
	
	}

}

/**
Definition of a MPI_Datatype for the struct "hash_e", in order to be able to send
the hash maps over the net
*/
void define_hash_e_mpi_type(MPI_Datatype *hash_e_type) {

    MPI_Datatype types[2] = { MPI_CHAR, MPI_INT };
    int blocklengths[2] = { WORDLENGHT, 1};
    hash_e e[1];
    MPI_Aint displacements[2];
    displacements[0] = (void *)&(e[0].value) - (void *)&e[0];
    displacements[1] = (void *)&(e[0].counter) - (void *)&e[0];

    MPI_Type_create_struct(2, blocklengths, displacements, types, hash_e_type);
    MPI_Type_commit(hash_e_type);

    return;
}