cd ..
mkdir OUTPUT

echo "Generating different input folders for the tests..."
np=0
while (( np++ < 20)); do
	num=$(( 48*$np ))
	sudo mkdir INPUT$num
	i=0
	while (( i++ < $((12*$np)) )); do
	  sudo cp INPUT/alice29.txt INPUT$num/"alice$i.txt"
	done
	i=0
	while (( i++ < $((12*$np)) )); do
	  sudo cp INPUT/asyoulik.txt INPUT$num/"asyoulik$i.txt"
	done
	i=0
	while (( i++ < $((12*$np)) )); do
	  sudo cp INPUT/lcet10.txt INPUT$num/"lcet$i.txt"
	done
	i=0
	while (( i++ < $((12*$np)) )); do
	  sudo cp INPUT/plrabn12.txt INPUT$num/"plrabn$i.txt"
	done
done