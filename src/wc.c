#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mpi.h"
#include "filepo.h"
#include "hashwc.h"

int main(int argc, char* argv[]) {
	// Time for benchmarking
	double globalStartTime, globalEndTime, mapStartTime, reduce_End_Time;
	globalStartTime = MPI_Wtime();

	// Input path
	if (argc != 2) {
		printf("Bisogna inserire un INPUT path come argomento\n");
		return -1;
	}
	char INPUT_PATH[50];
	sprintf(INPUT_PATH, "%s%s", argv[1], "/");
	
	// MPI Inizialization
	int rank, np;
	MPI_Status status;
	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    // MPI custom type
    MPI_Datatype MPI_HASH_E;
    define_hash_e_mpi_type(&MPI_HASH_E);

    // Hashmap
    hash_e *hash_ground;
    // Total hashmap, needed by the master to store the slaves' hmaps
    hash_e **hash_grounds;

   	// Everyone selects the files in the INPUT folder
    int num_files = get_num_files(INPUT_PATH);
   	FILE **files = malloc(num_files*sizeof(FILE *));
    get_files_from_dir(files, num_files, INPUT_PATH);
    long total_size;
    
    // MAP phase
    // The master needs to communicate to every slave 
    // which files (or parts of) need to work on
    if (rank == 0) {
    	mapStartTime = MPI_Wtime();

	    // Total size of the selected files
	    total_size = get_total_files_size(files, num_files);
	    /*
	    We create an assignment array that stores the starting and ending
    	file points for each processor, for example:
    	the couple "assignment_points[i] and assignment_points[i+1]"
    	indicates the starting and ending points for the processor i.
    	The values of the elements of the array are to be interpreted so:
    		- The integer part is the number of the file (the order is guaranteed)
    		- The decimal place is the percentage of the file to move at */
	    float assignment_points[np+1];
	    assign_files(assignment_points, files, num_files, total_size, np);

	    // We now need to communicate to the slaves their ranges
	    for (int i = 1; i < np; i++) {
	    	MPI_Send(&assignment_points[i], 2, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
	    }

	    // Time to allocate the space to store the upcoming hash tables (contiguously)
	    hash_e *total_hash_ground = malloc(np*WORDSNUMBER*sizeof(hash_e));
	    hash_grounds = malloc(np*sizeof(hash_e *));
	    for (int i = 0; i < np; i++) 
	    	hash_grounds[i] = &total_hash_ground[i*WORDSNUMBER];
	    hash_ground = hash_grounds[0];
	    init_hash_table(hash_ground);
	    // Also the master does a part of the job since there's nothing to do meanwhile
	    count_words_from_files(hash_ground, files, assignment_points[0], assignment_points[1]);

	}

	// The slaves receive the indications for their job and compute their hashmaps
	else {
		// Allocate the space for the hashmap meanwhile the master processes the files
		hash_ground = malloc(WORDSNUMBER*sizeof(hash_e));
		init_hash_table(hash_ground);

		// Let's receive from the master the start and end file points
		float points[2];
		MPI_Recv(&points, 2, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, &status);

		// The slaves fullfill their own hashmaps
		count_words_from_files(hash_ground, files, points[0], points[1]);

	}

	// Gathering fase: The slaves send to the master their own hashmap
	MPI_Gather(
	    hash_ground, 	//void* send_data,
	    WORDSNUMBER,	//int send_count,
	    MPI_HASH_E, 	//MPI_Datatype send_datatype,
	    *hash_grounds, 	// void* recv_data,
	    WORDSNUMBER, 	//int recv_count,
	    MPI_HASH_E, 	//MPI_Datatype recv_datatype,
	    0, 				//int root,
	    MPI_COMM_WORLD 	//MPI_Comm communicator
    );

	// REDUCE PHASE
	// The master merges the received hashmaps in only one (its own)
	if (rank == 0) {
		for (int i = 1; i < np; i++) {
			merge_h_tables(hash_ground, hash_grounds[i]);
		}
		// END of reduce phase
		reduce_End_Time = MPI_Wtime();
		
		// We should write everything somewhere
		FILE * results_file;
		char path_to_file[1024];
		sprintf(path_to_file, "%s%s", OUTPUT_PATH, OUTPUT_RESULT);
		printf("Writing the solution in %s\n", path_to_file);
		results_file = fopen (path_to_file,"w");

		for (int j = 0; j < WORDSNUMBER; j++)
			if (hash_ground[j].counter != 0)
				fprintf (results_file, "%s:\t\t%d\n",hash_ground[j].value, hash_ground[j].counter);

		// Times benchmarks output
		globalEndTime = MPI_Wtime();
		printf("%f\n", globalEndTime - globalStartTime);
		FILE * times_file;
		char path_to_file2[1024];
		sprintf(path_to_file2, "%s%sNP%d%d.txt", OUTPUT_PATH, BENCHMARKS_FILE, np, num_files);
		printf("Writing the benchmarks in %s\n", path_to_file2);
		times_file = fopen (path_to_file2,"w");
		fprintf(times_file, "Number of processors: %d\nNumber of files: %d\nTotal size = %ld\nGlobal time:\t%f\nInit time:\t\t%f\nOutput Writing Time:\t%f\n\nMAP-REDUCE execution time:\t%f\n",
		np, num_files, total_size, globalEndTime - globalStartTime, mapStartTime - globalStartTime, globalEndTime - reduce_End_Time, reduce_End_Time - mapStartTime );
		fclose(results_file);
		fclose(times_file);

	}

	// Close all files
	for (int i = 0; i < num_files; i++)
    	fclose((FILE *)files[i]);
	free(hash_ground);

	// The end
	MPI_Finalize();
	return 0;
}