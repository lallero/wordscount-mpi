#ifndef _HASHWC_H_
#define _HASHWC_H_
 
#define WORDLENGHT 32
#define WORDSNUMBER 200000

/**
Single hash element
it contains the word value and the counter of the occurrences
*/
typedef struct hash_e {
    char value[WORDLENGHT]; 	// the word
    int counter; 				// number of occurrences
} hash_e;

void init_hash_table(hash_e *hash_ground);
void define_hash_e_mpi_type(MPI_Datatype *hash_e_type);
void count_words_from_files(hash_e *hash_ground, FILE **files, float starting_point, float ending_point);
void merge_h_tables(hash_e *hg1, hash_e *hg2);

#endif