echo "Running Strong scalability benchmarks"
i=0
while (( i++ < 20 )); do
	sleep 1
	mpirun -np $i --hostfile machines ./wc INPUT960
	sleep 1
	mpirun -np $i --hostfile machines ./wc INPUT912
done

echo "Running Weak scalability benchmarks..."

i=0
while (( i++ < 20 )); do
	sleep 2
	mpirun -np $i --hostfile machines ./wc INPUT$(($i*48))
done

echo "Tutti i risultati sono nella cartella OUTPUT"
