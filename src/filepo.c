#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <wchar.h>
#include <locale.h>

#include "filepo.h"


/**
Returns the number of files in the folder given as input
*/
int get_num_files(char *input_path) {
	int num_files = 0;
	struct dirent *de;
	DIR *dr;

	if ((dr = opendir(input_path)) == NULL) {
		printf("%s\n", "Error opening the directory");
		return -1;
	} 

	//We count the number of files
	while ((de = readdir(dr)) != NULL) {
			num_files++;
	}
	num_files = num_files - 2; // minus current and parent folders links

	closedir(dr);
	return num_files;
}

// Needed for the sorting
int cstring_cmp(const void *a, const void *b) { 
    //const char **ia = (const char **)a; const char **ib = (const char **)b;
    return strcmp(a, b);
} 

/**
	It fulfills the array of file pointers "files" with the files in the folder
	given in input "input_path". They are added in alphabetical order.
	The files are left opened in read only mode
*/
void get_files_from_dir(FILE **files, int num_files, char *input_path) {

	struct dirent *de;
	DIR *dr;

	if ((dr = opendir(input_path)) == NULL) {
		printf("%s\n", "Error opening the directory");
		return ;
	} 

	//We create a list of file names
	int i = 0;
	char path_to_file[1024];
	char file_names[num_files][100];
	while ((de = readdir(dr)) != NULL) {
			if (!strcmp (de->d_name, ".") || (!strcmp (de->d_name, "..")))
            	continue;
            strcpy(file_names[i], de->d_name);
            i++;
    }
	closedir(dr);

    // We need to sort the files in order to be sure every slave has the same files order
    qsort(file_names, num_files, sizeof(*file_names), cstring_cmp);

    // Creating of the array of opened files
    for ( i = 0; i < num_files; i++) {
            // Creating the correct path to the single current file
            sprintf(path_to_file, "%s%s", input_path, file_names[i]);
            files[i] = fopen(path_to_file, "r");
            if (files[i] == NULL)
            	return ;
	}
	return; 
}

/**
Returns the number of bytes/size of the file in input
*/
long get_file_size (FILE *file) {

	fseek(file, 0L, SEEK_END);
	long size = ftell(file);
	fseek(file, 0L, SEEK_SET);
	return size;
}

/**
Returns the number of bytes/sizes of an array of files
*/
long get_total_files_size (FILE **files, int num_files) {

	int i;
	long total_size = 0;
	for (i = 0; i < num_files; i++) {
		total_size = total_size + get_file_size(files[i]);
	}

	return total_size;
}

/**
	We divide the workload represented by an array files into n_proc sets of files.
	To do a perfect division we truncate the files that are too big.
	We output an array of float "assignment_points" where every couple of elements (i, i+1)
	is the range of the files for the proc i. In details:
		- the integer part indicates the file number
		- the decimal part indicates the percetage of file
*/
void assign_files (float *assignment_points, FILE **files, int num_files, long total_size, int np) {
	
	long chunk_size = total_size/(np);
	long extra_size = total_size%(np);
	if (extra_size != 0)
		chunk_size++;

	int current_file = 0;
	int current_proc = 0;
	long file_size; // The size of the current file selected
	long added_size = 0; // The part of files (in bytes) already assigned to the current proc
	long remaining_size = -1; // The remaining part (in bytes) of a file not yet assigned
	float point_index = 0; // Represents the index of the reached file, it may refer to a portion (percentage) of it

	assignment_points[current_proc] = point_index;
	while (current_file < num_files) {
		file_size = get_file_size(files[current_file]);		
		if (remaining_size == -1) 
			remaining_size = file_size;

		// In this case the current file (or what remains) fits abuntantly
		// in the current proc chunk, so we take all the entire remaining file
		if (remaining_size <= chunk_size - added_size) {
			
			point_index = ((int) point_index) + 1;
			added_size = added_size + remaining_size;

			// Very rare case: the file size perfectly fits the remaining req
			if (remaining_size == chunk_size - added_size) {
				current_proc++;
				added_size = 0;
				assignment_points[current_proc] = point_index;
			}

			current_file++;
			remaining_size = -1;
			continue;
		} 

		// The remaining file is too big and we need to split it
		if (remaining_size > chunk_size - added_size) {
			
			// We take from the current file the necessary 
			// amount of bytes to fill the proc's need		
			long needed = chunk_size - added_size;

			// A proc is completely been assigned, let's switch to the next one
			current_proc++;
			
			// Let's now calculate the percentage of file we are going to take
			float percentage = (float)needed/(float)file_size;

			// So let's add it to our assigned points marker
			point_index = point_index + percentage;
			assignment_points[current_proc] = point_index;

			// let the alghoritm continue
			remaining_size = remaining_size - needed;	
			added_size = 0;

		}
	}
	// The last file must be taken enterely
	assignment_points[np] = num_files;
	return;
}