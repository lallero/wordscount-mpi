# MPI Parallel Words Count

## Problem statement
We have a variable number of text files in input. The required output is a list of unique words followed by their occurrences in all the files.
We are required to develop a program able to run in parallel mode, to be used in a cluster. In order to obtain this the used technology are the OpenMPI libraries in a C program.

## Assumptions
1. The input files considered are the ones being in an input folder next to *src* folder and it has to be specified as argument
2. Every process sees the list of all the files. Sending the files through MPI would be inefficient compared to better ad-hoc solutions (ftp, scp, ...)

## Program Structure

### Problem Considerations and General Idea
The most important concern is about the division of the workload per process. Since the input is not known a priori, we could expect any form of text disposition: for example we may have an unique very large file with all the words in a unique line or we may have hundreds of small files (see the image below to get an idea). The division of the workload will then be based on the number of bytes of the files (and not on the number of lines, e.g.). Since the files get brutally truncated it is probable that some words could be split: this is not desirable, so, in this case, the markers of file truncation point will be moved few bytes forward. This is the initial task of the MASTER. Once it has designated the division arrangement it communicates to each slave their chunks information (which files and how much of them). The slaves receive the instructions and execute the wordscounting: the words are inserted and counted in a hash table. Once the calculations are terminated the slaves send the hashmaps to the master, which will merge them all in a unique one, that will be the solution of the problem.

### Data Structure
The words are inserted in a **hash table** where each hash element has a "value" field containing the word string and a "counter" field containing the occurrences of that word. The collision problem is solved via the *linear probing strategy*. This strategy fits very well with the need to have a fixed long contiguous array of elements in order to be sent via MPI. An implementation via linked lists would have required one more step to copy all the list, at the end of the calculations, in a contiguous array. This may be more dynamic but maybe less efficient. Setting a good number of elements of the hash table would be anyway almost perfect for almost any kind of common text. In this program, it has been set to 200000 that is the maximum of unique words we expect. This value can always be tuned for more specific needs.
NOTE: the size of a hashtable is (size_of_hash_element x number_of_elements) = 36 x 200000 = 7200000 bytes ≈ 6,87 MB and it is irrelevant for any modern cluster communication speed.

### File Division
The wanted chunks get projected on the files division: for each process it has to be decided a starting file point marker, that indicates the point *in* a file where to start reading and an analogous ending file point marker. As in the picture, a file point marker (red lines) is both the starting one for the process on the right and the ending one for the process on the left (when they are not on the borders).

![Files division](https://i.imgur.com/D0s9R81.png)


### Phases
The algorithm could then be structured in these phases:
#### 1 - MAP
The MASTER analyzes the files and decides to divide the workload and assign **total_files_size/number_of_processes** bytes to each process (chunk). It calculates the starting and ending file point markers according to the seen strategy and it communicates to each slave this couple of values via *MPI_Send()* (and *MPI_Recv()* for the slaves).
#### 2 - Calculation
The slaves interpret the file point markers received, move them eventually by few bytes forward to avoid words truncation, and start counting words. The words get stored in a hash table.
#### 3 - REDUCE
Once the slaves (and the master, that also contributes to the calculations) have finished their job, all the hash tables get **gathered** (via *MPI_Gather()*) in the master memory. The sending via MPI is possible thanks to the definition of a custom *MPI data type* constructed to be the analogous of the hash element built via *struct*. The master then merges all the hash tables in a unique one. It finally writes the solution into an output text file.

### Implementation details
The program is composed by 3 .c files
1. **filepo.c** contains the functions to handle the files (reading, measuring, assigning)
2. **hashwc.c** contains the functions to handle the hash tables and to wordscount
3. **wc.c** contains the main() which handles the MPI operations, allocate the memory for the hash tables, invokes the files and wordscounting operations, write the output on a file and take the time measurements for the benchmarks.

filepo.c and hashwc.c have a related header file .h

## Building and Running
1. Building requires to invoke *make* in the src directory. All the compiling parameters are set in the *makefile* file.
2. Running requires the existence of an output folder next to *src* called "OUTPUT" and the invokation of *mpirun -np <#np> ./wc input_folder* where *np* is the number of the wanted processes and *input_folder* is a folder, next to *src*, containing the input files. (*mpirun -np <#np> --oversubscribe ./wc input_folder* to execute the program when *<#np>* is bigger than the number of available processors. Useful for testing in standalone executions)

## Benchmarks
The instance type chosen for the benchmarking tests is the **m4.xlarge** by amazon aws, which comes with 4 vCPUs. The maximum number of instances will be 5. So, we will effectuate a total of 20 tests, selecting an increasing number of processes from 1 to 20 (4x5).

### Environment and Execution
The environment for testing is built easily:
1. Execute *ssh-keygen* on the master and fulfill the *install.sh* file with the created keys
2. Run *install.sh* on every machine (it installs openMPI and creates an user *pcpc*)
3. On every machine switch to pcpc user with `su - pcpc` and get the project (all the folder *wordscount-mpi*) via git or via scp
4. On every machine run *create_input.sh* that creates a set of input folder containing an increasing number of files
5. Compile the program on the master and send the *wc* executable file to all the slaves (needs to be placed in *src* folder)
6. In the master's *src* folder create a *machines* file containing the public IPs of the slaves followed by "slots=4" (where 4 is the number of processors of the machines)
7. Execute **benchmarks.sh**. All the benchmarks results will be placed in the *OUTPUT* folder

NOTE: The *benchmarks.sh* script executes both weak and strong benchmarks tests and it **strongly relies** on the INPUT folders created by *create_input.sh*.


### Strong Scalability
Strong scalability is tested by using the same (large) input with an increasing number of processes. Here, the test has been repeated for 2 inputs

#### Input
1. The first input (the red one in the chart) is composed of 960 files for a total of 271.4 MB
2. The second one (the blue one) is composed of 912 files for a total of 257.9 MB

#### Chart
![strongs](https://i.imgur.com/16AJhq5.png)

#### Rationale
First of all, we see that the program does scale and does gain in performance increasing the number of processors. Secondly, the chart reveals that until we use 14 processors the gain is visible but from 15 to 20 the gain is very poor. This was expected because the input per process becomes very small and the weight of the network limitation starts owning (**1**). We notice that the number of the on-network communications increases linearly with the number of processes. The explanation of **1** is that when the CPU work per process decreases proportionally with the number of processes, the overhead of communication increases linearly and it's obvious that, for any input, from a certain point on, the communications limitation becomes the upper bound for the performances (**network bounded**).

### Weak Scalability
Weak Scalability is tested by keeping the workload per process fixed while increasing the number of the processors

#### Input
1. The input per process consists of 48 files for a total of 13.6 MB

#### Chart
![weaks](https://i.imgur.com/uoBDps4.png)

#### Rationale
An utopic perfect result would have been a constant function. The function obtained here is not constant but it raises slowly enough to consider the test successful: from 1 to 20 processors the time just increases of a factor of 2,88. Observations are needed here to explain the result: the input per processor was anyway low and the same network limitation described for the strong scalability tests are worth here too. As before, the network communications increases linearly and the performance becomes network bounded. We could strongly hypothesize that repeating the test for bigger input-per-process would result in a lower factor of time increase because the overall time would be increased by the higher CPU-work needed. Another note is about the entity of the communication weight: the hashtable to be sent is size-fixed.

## Conclusions
Improvements for the future:

 - Avoid assumption **2**: let the program send to the slaves the files via more efficient ways